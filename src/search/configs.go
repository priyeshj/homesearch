package search

import (
    "log"
    "os"
    "path/filepath"
    "html/template"
)

// Global Inits
var (
    // I would usually put this in a non-vcs config, but putting it here due to time constraints
    apiKey        = "X1-ZWz1dyb53fdhjf_6jziz"
    apiURL        = "http://www.zillow.com/webservice/GetSearchResults.htm"
    errLog        = log.New(os.Stdout, "ERROR ", log.LstdFlags)

    cwd, _        = os.Getwd()
    tmplts        = template.Must(template.ParseFiles(filepath.Join(cwd, "/src/tmplts/home.html")))
)

// Constants
const (
    MaxIdleConnections int = 20
    RequestTimeout     int = 10
)
