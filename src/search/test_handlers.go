package search

import (
    "fmt"
    "testing"
    "net/http"
    "net/http/httptest"
    "io/ioutil"
    "net/url"
    "bytes"
)


func TestHomeHandlerGet(zt *testing.T) {
    fmt.Printf("Testing Home Handler")

    // Setup test request
    var testRequest *http.Request
    testRequest, err := http.NewRequest("GET", "/home", nil)
    if err != nil {
        zt.Fatal(err)
    }

    // Setup recorder for response and execute
    testWriter := httptest.NewRecorder()
    HomeHandler(testWriter, testRequest)

    // Validating expectations
    if status := testWriter.Code; status != http.StatusOK {
        zt.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    expectedContentType := "text/html"
    if expectedContentType != testWriter.HeaderMap.Get("Content-Type") {
        zt.Errorf("GET /home Failed to validate content type: %v %v", expectedContentType, resp.Header.Get("Content-Type"))
    }

    body, _ := ioutil.ReadAll(testWriter.Body)
    fmt.Println(string(body))

}

func TestHomeHandlerPost(zt *testing.T) {
    fmt.Printf("Testing Home Handler")

    // Setup test request
    var testRequest *http.Request
    data := url.Values{}
    data.Set("address", "5000 S Centinela ave")
    data.Add("citystatezip", "90066")
    testRequest, err := http.NewRequest("POST", "/home", bytes.NewBufferString(data.Encode()))
    if err != nil {
        zt.Fatal(err)
    }

    // Setup recorder for response and execute
    testWriter := httptest.NewRecorder()
    HomeHandler(testWriter, testRequest)

    if status := testWriter.Code; status != http.StatusOK {
        zt.Errorf("handler returned wrong status code: got %v want %v",
            status, http.StatusOK)
    }

    expectedContentType := "text/html"
    if expectedContentType != testWriter.HeaderMap.Get("Content-Type") {
        zt.Errorf("GET /home Failed to validate content type: %v %v", expectedContentType, resp.Header.Get("Content-Type"))
    }

    body, _ := ioutil.ReadAll(testWriter.Body)
    fmt.Println(string(body))

}