package search

import (
    "net/http"
    // zillow "github.com/jmank88/zillow"
    // Credits: ^ was referenced and partially used to create zillow-wrapper
)

// Home page
func HomeHandler(writer http.ResponseWriter, req *http.Request) {

    var errorMessage = `Unable to serve results due to an internal error,
please contact the system administrator for more details`

    switch req.Method {
        case "GET":
            // Serve the home page.
            err := tmplts.ExecuteTemplate(writer, "home.html", nil)
            if err != nil {
                errLog.Printf("Failed to render home template: %s", err.Error())
                http.Error(writer, errorMessage, http.StatusInternalServerError)
            }

        case "POST":
            // Search
            var zreq ZillowRequest
            var zresp ZillowResponse
            var err error

            req.ParseForm()
            zreq.Address = req.Form.Get("address")
            zreq.CityStateZip = req.Form.Get("citystatezip")
            zreq.Rentzestimate = true

            zresp, err = getSearchResults(zreq)
            if err != nil {
                errLog.Printf("Failed to get search results: %s", err.Error())
                http.Error(writer, errorMessage, http.StatusInternalServerError)
                return
            }

            err = tmplts.ExecuteTemplate(writer, "home.html", zresp)
            if err != nil {
                errLog.Printf("Failed to render home search results template: %s", err.Error())
                http.Error(writer, errorMessage, http.StatusInternalServerError)
                return
            }

        default:
            // Give an error message.
            http.Error(writer, "Invalid request method.", 405)
    }

}
