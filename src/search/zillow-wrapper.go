package search

import (
    "net/http"
    "net/url"
    "encoding/xml"
    "strconv"
)

// Zillow request struct
type ZillowRequest struct {
    Address       string `xml:"address"`
    CityStateZip  string `xml:"citystatezip"`
    Rentzestimate bool   `xml:"rentzestimate"`
}

// Zillow response structs
type Message struct {
    Text         string `xml:"text"`
    Code         int    `xml:"code"`
    LimitWarning bool   `xml:"limit-warning"`
}

type Links struct {
    XMLName xml.Name `xml:"links"`

    HomeDetails   string `xml:"homedetails"`
    GraphsAndData string `xml:"graphsanddata"`
    MapThisHome   string `xml:"mapthishome"`
    MyZestimator  string `xml:"myzestimator"`
    Comparables   string `xml:"comparables"`
}

type Address struct {
    Street    string `xml:"street"`
    Zipcode   string `xml:"zipcode"`
    City      string `xml:"city"`
    State     string `xml:"state"`
    Latitude  string `xml:"latitude"`
    Longitude string `xml:"longitude"`
}

type Value struct {
    Currency string `xml:"currency,attr"`
    Value    int    `xml:",chardata"`
}

type Zestimate struct {
    Amount      Value  `xml:"amount"`
    LastUpdated string `xml:"last-updated"`
    // TODO(pedge): fix
    //ValueChange ValueChange `xml:"valueChange"`
    Low        Value  `xml:"valuationRange>low"`
    High       Value  `xml:"valuationRange>high"`
    Percentile string `xml:"percentile"`
}

type RealEstateRegion struct {
    XMLName xml.Name `xml:"region"`

    ID                  string  `xml:"id,attr"`
    Type                string  `xml:"type,attr"`
    Name                string  `xml:"name,attr"`
    ZIndex              string  `xml:"zindexValue"`
    ZIndexOneYearChange float64 `xml:"zindexOneYearChange"`
    // Links
    Overview       string `xml:"links>overview"`
    ForSaleByOwner string `xml:"links>forSaleByOwner"`
    ForSale        string `xml:"links>forSale"`
}

type SearchResult struct {
    XMLName xml.Name `xml:"result"`
    Zpid string `xml:"zpid"`
    Links           Links              `xml:"links"`
    Address         Address            `xml:"address"`
    Zestimate       Zestimate          `xml:"zestimate"`
    LocalRealEstate []RealEstateRegion `xml:"localRealEstate>region"`
}

type ZillowResponse struct {
    XMLName xml.Name `xml:"searchresults"`
    Request ZillowRequest `xml:"request"`
    Message Message       `xml:"message"`
    Results []SearchResult `xml:"response>results>result"`
}

// Get search details from zillow to render on home page
func getSearchResults(request ZillowRequest) (ZillowResponse, error) {

    values := url.Values{
        "zws-id":        {apiKey},
        "address":       {request.Address},
        "citystatezip":  {request.CityStateZip},
        "rentzestimate": {strconv.FormatBool(request.Rentzestimate)},
    }
    var result ZillowResponse

    completeURL := apiURL + "?" + values.Encode()
    resp, err := http.Get(completeURL)
    if err != nil {
        errLog.Printf("Failed to get search results from zillow: %s", err.Error())
        return result, err
    }

    err = xml.NewDecoder(resp.Body).Decode(&result)
    if err != nil {
        errLog.Printf("Failed to decode search results from zillow: %s", err.Error())
        return result, err
    }

    return result, nil
}
