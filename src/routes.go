package main

import (
    "net/http"
    "search"
)

// URL Router
func main() {
    //http.HandleFunc("/../js/", search.ServeResource)
    //http.HandleFunc("/../css/", search.ServeResource)
    http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("src/css"))))
    http.Handle("/js/", http.StripPrefix("/js/", http.FileServer(http.Dir("src/js"))))
    http.HandleFunc("/home", search.HomeHandler)
    http.ListenAndServe(":8080", nil)
}
