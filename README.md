===========
Home Search
===========


Problem Statement
-----------------

Goal: Create a simple web app that allows a user to enter an address for a home and then see details for that home from Zillow's GetSearchResults API (API key: X1-ZWz1dyb53fdhjf_6jziz). The output should be a cleanly-displayed representation of whatever came back in the API. We're not testing your design skills here and so we're not looking for beauty, but at the same time, try to make it something you'd be proud to show friends and family. If the API responds with an error for some reason, the user should be informed and not left hanging.
Unit testing: Not necessary, although it'd be great if your functions were designed with unit testing in mind in case you were asked to add it later for some reason.

Language preference: It's preferable but not required, if this was created with either Go or .NET Core for the server-side. You can use any libraries you'd like for both the server-side and the client-side. Keep in mind though that, especially on the front end, the more libraries you use, the more they can obscure your knowledge of the fundamentals. Use your own discretion here.

Hosting: For the app itself, it'd be great if you could host it on AWS, heroku, Azure, or another cloud platform. For the code, it'd be great if you could host it on Github, Bitbucket, or the like. Although not preferred, you can also just email the code as a zip.

Time: We understand we're not your only choice, and so we want to be respectful of your time. Please don't spend more than a few hours on this; if you hit your stopping point, just send in what you have.


Backend
-------

Frontend
--------

Improvements
------------